/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package libp2pnet

import (
	"bufio"
	"context"
	"crypto/x509"
	"crypto/x509/pkix"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"chainmaker.org/chainmaker/chainmaker-net-common/common"
	"chainmaker.org/chainmaker/chainmaker-net-common/common/priorityblocker"
	"chainmaker.org/chainmaker/chainmaker-net-common/gmtlssupport"
	"chainmaker.org/chainmaker/chainmaker-net-common/tlssupport"
	"chainmaker.org/chainmaker/chainmaker-net-common/utils"
	"chainmaker.org/chainmaker/chainmaker-net-libp2p/datapackage"
	cmx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/common/v2/helper"
	libP2pPubSub "chainmaker.org/chainmaker/libp2p-pubsub"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	api "chainmaker.org/chainmaker/protocol/v2"
	"github.com/gogo/protobuf/proto"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	gmx509 "github.com/tjfoc/gmsm/x509"
)

const (
	// DefaultLibp2pListenAddress is the default address that libp2p will listen on.
	DefaultLibp2pListenAddress = "/ip4/0.0.0.0/tcp/0"
	// DefaultLibp2pServiceTag is the default service tag for discovery service finding.
	DefaultLibp2pServiceTag = "chainmaker-libp2p-net"
)

var (
	ErrorPubSubNotExist      = errors.New("pub-sub service not exist")
	ErrorPubSubExisted       = errors.New("pub-sub service existed")
	ErrorTopicSubscribed     = errors.New("topic has been subscribed")
	ErrorSendMsgIncompletely = errors.New("send msg incompletely")
	ErrorNotConnected        = errors.New("node not connected")
	ErrorNotBelongToChain    = errors.New("node not belong to chain")
)

// MsgPID is the protocol.ID of chainmaker net msg.
const MsgPID = protocol.ID("/ChainMakerNetMsg/1.0.0/")

// DefaultMessageSendTimeout is the default timeout for sending msg.
const DefaultMessageSendTimeout = 3 * time.Second

// compressThreshold is the default threshold value for enable compress net msg bytes. Default value is 1M.
const compressThreshold = 1024 * 1024

const pubSubWhiteListChanCap = 50
const pubSubWhiteListChanQuitCheckDelay = 10

var _ api.Net = (*LibP2pNet)(nil)

// LibP2pNet is an implementation of net.Net interface.
type LibP2pNet struct {
	compressMsgBytes          bool
	lock                      sync.RWMutex
	startUp                   bool
	netType                   api.NetType
	ctx                       context.Context // ctx context.Context
	libP2pHost                *LibP2pHost     // libP2pHost is a LibP2pHost instance.
	messageHandlerDistributor *MessageHandlerDistributor
	pktAdapter                *pktAdapter
	priorityController        *priorityblocker.Blocker

	pubSubs          sync.Map                      // map[string]*LibP2pPubSub , map[chainId]*LibP2pPubSub
	subscribedTopics map[string]*topicSubscription // map[chainId]*topicSubscription
	subscribeLock    sync.Mutex

	prepare *LibP2pNetPrepare // prepare contains the base info for the net starting.

	log api.Logger
}

func (ln *LibP2pNet) SetCompressMsgBytes(enable bool) {
	ln.compressMsgBytes = enable
}

type topicSubscription struct {
	m map[string]*libP2pPubSub.Subscription
}

func (ln *LibP2pNet) peerChainIdsRecorder() *common.PeerIdChainIdsRecorder {
	return ln.libP2pHost.peerChainIdsRecorder
}

// NewLibP2pNet create a new LibP2pNet instance.
func NewLibP2pNet(log api.Logger) (*LibP2pNet, error) {
	ctx := context.Background()
	host := NewLibP2pHost(ctx, log)
	net := &LibP2pNet{
		startUp:                   false,
		netType:                   api.Libp2p,
		ctx:                       ctx,
		libP2pHost:                host,
		messageHandlerDistributor: newMessageHandlerDistributor(),
		pktAdapter:                nil,
		pubSubs:                   sync.Map{},
		subscribedTopics:          make(map[string]*topicSubscription),

		prepare: &LibP2pNetPrepare{
			listenAddr:               DefaultLibp2pListenAddress,
			bootstrapsPeers:          make(map[string]struct{}),
			chainTrustRootCertsBytes: make(map[string][][]byte, 0),
			maxPeerCountAllow:        DefaultMaxPeerCountAllow,
			peerEliminationStrategy:  int(LIFO),

			blackAddresses: make(map[string]struct{}),
			blackPeerIds:   make(map[string]struct{}),
			pktEnable:      false,
		},
		log: log,
	}
	return net, nil
}

func (ln *LibP2pNet) Prepare() *LibP2pNetPrepare {
	return ln.prepare
}

// GetNodeUid is the unique id of node.
func (ln *LibP2pNet) GetNodeUid() string {
	return ln.libP2pHost.Host().ID().Pretty()
}

// isSubscribed return true if the given topic given has subscribed.Otherwise, return false.
func (ln *LibP2pNet) isSubscribed(chainId string, topic string) bool {
	topics, ok := ln.subscribedTopics[chainId]
	if !ok {
		return false
	}
	_, ok = topics.m[topic]
	return ok
}

// getPubSub return the LibP2pPubSub instance which uid equal the given chainId .
func (ln *LibP2pNet) getPubSub(chainId string) (*LibP2pPubSub, bool) {
	ps, ok := ln.pubSubs.Load(chainId)
	var pubsub *LibP2pPubSub = nil
	if ok {
		pubsub = ps.(*LibP2pPubSub)
	}
	return pubsub, ok
}

// InitPubSub will create new LibP2pPubSub instance for LibP2pNet with setting pub-sub uid to the given chainId .
func (ln *LibP2pNet) InitPubSub(chainId string, maxMessageSize int) error {
	_, ok := ln.getPubSub(chainId)
	if ok {
		return ErrorPubSubExisted
	}
	if maxMessageSize <= 0 {
		maxMessageSize = DefaultLibp2pPubSubMaxMessageSize
	}
	ps, err := NewPubsub(chainId, ln.libP2pHost, maxMessageSize)
	if err != nil {
		ln.log.Errorf("[Net] new pubsub failed, %s", err.Error())
		return err
	}
	ln.pubSubs.Store(chainId, ps)
	if ln.startUp {
		if err = ps.Start(); err != nil {
			return err
		}
		ln.reloadChainPubSubWhiteList(chainId)
	}
	return nil
}

// BroadcastWithChainId broadcast a msg to the given topic of the target chain which id is the given chainId .
func (ln *LibP2pNet) BroadcastWithChainId(chainId string, topic string, data []byte) error {
	topic = chainId + "_" + topic
	bytes := data
	pubSub, ok := ln.getPubSub(chainId)
	if !ok {
		return ErrorPubSubNotExist
	}
	return pubSub.Publish(topic, bytes) //publish msg
}

// getSubscribeTopicMap
func (ln *LibP2pNet) getSubscribeTopicMap(chainId string) *topicSubscription {
	topics, ok := ln.subscribedTopics[chainId]
	if !ok {
		ln.subscribedTopics[chainId] = &topicSubscription{
			m: make(map[string]*libP2pPubSub.Subscription),
		}
		topics = ln.subscribedTopics[chainId]
	}
	return topics
}

// SubscribeWithChainId subscribe the given topic of the target chain which id is
// the given chainId with the given sub-msg handler function.
func (ln *LibP2pNet) SubscribeWithChainId(chainId string, topic string, handler api.PubSubMsgHandler) error {
	ln.subscribeLock.Lock()
	defer ln.subscribeLock.Unlock()
	topic = chainId + "_" + topic
	// whether pubsub existed
	pubsub, ok := ln.getPubSub(chainId)
	if !ok {
		return ErrorPubSubNotExist
	}
	// whether subscribed
	if ln.isSubscribed(chainId, topic) { //检查topic是否已被订阅
		return ErrorTopicSubscribed
	}
	topicSub, err := pubsub.Subscribe(topic) // subscribe the topic
	if err != nil {
		return err
	}
	// add subscribe info
	topics := ln.getSubscribeTopicMap(chainId)
	topics.m[topic] = topicSub
	// run a new goroutine to handle the msg from the topic subscribed.
	go func() {
		defer func() {
			if err := recover(); err != nil {
				if !ln.isSubscribed(chainId, topic) {
					return
				}
				ln.log.Errorf("[Net] subscribe goroutine recover err, %s", err)
			}
		}()
		ln.topicSubLoop(chainId, topicSub, topic, handler)
	}()
	return nil
}

func (ln *LibP2pNet) topicSubLoop(
	chainId string,
	topicSub *libP2pPubSub.Subscription,
	topic string,
	handler api.PubSubMsgHandler) {
	for {
		message, err := topicSub.Next(ln.ctx)
		if err != nil {
			if err.Error() == "subscription cancelled" {
				ln.log.Warn("[Net] ", err)
				break
			}
			//logger
			ln.log.Errorf("[Net] subscribe next failed, %s", err.Error())
		}
		if message == nil {
			return
		}
		// if author of the msg is myself , just skip and continue
		if message.ReceivedFrom == ln.libP2pHost.host.ID() || message.GetFrom() == ln.libP2pHost.host.ID() {
			continue
		}
		// if author of the msg not belong to this chain, drop it
		if !ln.peerChainIdsRecorder().IsPeerBelongToChain(message.GetFrom().Pretty(), chainId) {
			continue
		}
		bytes := message.GetData()
		ln.log.Debugf("[Net] receive subscribed msg(topic:%s), data size:%d", topic, len(bytes))
		// call handler
		if err = handler(message.GetFrom().Pretty(), bytes); err != nil {
			ln.log.Warnf("[Net] call subscribe handler failed, %s ", err)
		}
	}
}

// CancelSubscribeWithChainId cancel subscribing the given topic of the target chain which id is the given chainId.
func (ln *LibP2pNet) CancelSubscribeWithChainId(chainId string, topic string) error {
	ln.subscribeLock.Lock()
	defer ln.subscribeLock.Unlock()
	topic = chainId + "_" + topic
	_, ok := ln.getPubSub(chainId)
	if !ok {
		return ErrorPubSubNotExist
	}
	topics := ln.getSubscribeTopicMap(chainId)
	if topicSub, ok := topics.m[topic]; ok {
		topicSub.Cancel()
		delete(topics.m, topic)
	}
	return nil
}

func (ln *LibP2pNet) isConnected(node string) (bool, peer.ID, error) {
	isConnected := false
	peerId := node
	if ln.libP2pHost.certPeerIdMapper != nil {
		var err error
		peerIdNew, err := ln.libP2pHost.certPeerIdMapper.FindPeerIdByCertId(node)
		if err == nil && peerIdNew != "" {
			// not found
			peerId = peerIdNew
		}
	}
	pid, err := peer.Decode(peerId) // peerId
	if err != nil {
		return false, pid, err
	}
	isConnected = ln.libP2pHost.HasConnected(pid)
	return isConnected, pid, nil

}

func (ln *LibP2pNet) sendMsg(chainId string, pid peer.ID, msgFlag string, data []byte) error {
	stream, err := ln.libP2pHost.peerStreamManager.borrowPeerStream(pid)
	if err != nil {
		return err
	}
	var bytes []byte
	pkg := datapackage.NewPackage(utils.CreateProtocolWithChainIdAndFlag(chainId, msgFlag), data)
	dataLen := len(data)
	// whether compress bytes
	isCompressed := []byte{byte(0)}
	if ln.compressMsgBytes && dataLen > compressThreshold {
		bytes, err = pkg.ToBytes(true)
		if err != nil {
			ln.log.Debugf("[Net] marshal data package to bytes failed, %s", err.Error())
			return err
		}
		isCompressed[0] = byte(1)
	} else {
		bytes, err = pkg.ToBytes(false)
		if err != nil {
			ln.log.Debugf("[Net] marshal data package to bytes failed, %s", err.Error())
			return err
		}
	}
	lengthBytes := utils.IntToBytes(len(bytes))
	writeBytes := append(lengthBytes, bytes...)
	size, err := stream.Write(writeBytes) // send data
	if err != nil {
		ln.libP2pHost.peerStreamManager.dropPeerStream(pid, stream)
		return err
	}
	if size < len(writeBytes) { //发送不完整
		ln.libP2pHost.peerStreamManager.dropPeerStream(pid, stream)
		return ErrorSendMsgIncompletely
	}
	ln.libP2pHost.peerStreamManager.returnPeerStream(pid, stream)
	return nil
}

// SendMsg send a msg to the given node belong to the given chain.
func (ln *LibP2pNet) SendMsg(chainId string, node string, msgFlag string, data []byte) error {
	if node == ln.GetNodeUid() {
		ln.log.Warn("[Net] can not send msg to self")
		return nil
	}
	if ln.priorityController != nil {
		ln.priorityController.Block(msgFlag)
	}

	isConnected, pid, _ := ln.isConnected(node)
	if !isConnected { // is peer connected
		return ErrorNotConnected // node not connected
	}
	// is peer belong to this chain
	if !ln.prepare.isInsecurity && !ln.libP2pHost.peerChainIdsRecorder.IsPeerBelongToChain(pid.Pretty(), chainId) {
		return ErrorNotBelongToChain
	}
	// whether pkt adapter enable
	if ln.pktAdapter != nil {
		return ln.pktAdapter.sendMsg(chainId, pid, msgFlag, data)
	}
	return ln.sendMsg(chainId, pid, msgFlag, data)
}

func (ln *LibP2pNet) registerMsgHandle() error {
	var streamReadHandler = func(stream network.Stream) {
		streamReadHandlerFunc := NewStreamReadHandlerFunc(ln)
		go streamReadHandlerFunc(stream)
	}
	ln.libP2pHost.host.SetStreamHandler(MsgPID, streamReadHandler) // set stream handler for libP2pHost.
	return nil
}

// NewStreamReadHandlerFunc create new function for listening stream reading.
func NewStreamReadHandlerFunc(ln *LibP2pNet) func(stream network.Stream) {
	return func(stream network.Stream) {
		id := stream.Conn().RemotePeer().Pretty() // sender peer id
		reader := bufio.NewReader(stream)
		for {
			length := readMsgLength(reader, stream, ln.log)
			if length == -1 {
				break
			} else if length == -2 {
				continue
			}

			data, ret := readMsgReadDataRealWanted(reader, stream, length, ln.log)
			if ret == -1 {
				break
			} else if ret == -2 {
				continue
			}
			if len(data) == 0 {
				time.Sleep(500 * time.Millisecond)
				continue
			}
			var pkg datapackage.Package
			if err := pkg.FromBytes(data); err != nil {
				ln.log.Debugf("[Net] unmarshal data package from bytes failed, %s", err.Error())
				continue
			}
			chainId, flag := utils.GetChainIdAndFlagWithProtocol(pkg.Protocol())
			if !ln.peerChainIdsRecorder().IsPeerBelongToChain(id, chainId) {
				ln.log.Debugf("[Net] sender not belong to chain. drop message. (chainId:%s, sender:%s)",
					chainId, id)
				continue
			}
			handler := ln.messageHandlerDistributor.handler(chainId, flag)
			if handler == nil {
				ln.log.Warnf("[Net] handler not registered. drop message. (chainId:%s, flag:%s)", chainId, flag)
				continue
			}
			readMsgCallHandler(id, pkg.Payload(), handler, ln.log)
		}
	}
}

func readData(reader *bufio.Reader, length int) ([]byte, error) {
	batchSize := 4000
	result := make([]byte, 0)
	for length > 0 {
		if length < batchSize {
			batchSize = length
		}
		bytes := make([]byte, batchSize)
		c, err := reader.Read(bytes)
		if err != nil {
			return nil, err
		}
		length = length - c
		result = append(result, bytes[0:c]...)
	}
	return result, nil
}

func readMsgReadDataErrCheck(err error, stream network.Stream, log api.Logger) int {
	if strings.Contains(err.Error(), "stream reset") {
		_ = stream.Reset()
		return -1
	}
	log.Warnf("[Net] read stream failed, %s", err.Error())
	return -2
}

func readMsgLength(reader *bufio.Reader, stream network.Stream, log api.Logger) int {
	lengthBytes := make([]byte, 8)
	_, err := reader.Read(lengthBytes)
	if err != nil {
		return readMsgReadDataErrCheck(err, stream, log)
	}
	length := utils.BytesToInt(lengthBytes)
	return length
}

func readMsgReadDataRealWanted(reader *bufio.Reader, stream network.Stream, length int, log api.Logger) ([]byte, int) {
	data, err := readData(reader, length)
	if err != nil {
		return nil, readMsgReadDataErrCheck(err, stream, log)
	}
	return data, 0
}

func readMsgCallHandler(id string, data []byte, handler api.DirectMsgHandler, log api.Logger) {
	go func(id string, data []byte, handler api.DirectMsgHandler) {
		/*defer func() {
			if err := recover(); err != nil {
				log.Error("[Net] stream read handler func call handler recover failed, %s", err)
			}
		}()*/
		err := handler(id, data) // call handler
		if err != nil {
			log.Warnf("[Net] stream read handler func call handler failed, %s", err.Error())
		}
	}(id, data, handler)
}

// DirectMsgHandle register a DirectMsgHandler for handling msg received.
func (ln *LibP2pNet) DirectMsgHandle(chainId string, msgFlag string, handler api.DirectMsgHandler) error {
	return ln.messageHandlerDistributor.registerHandler(chainId, msgFlag, handler)
}

// CancelDirectMsgHandle unregister a DirectMsgHandler for handling msg received.
func (ln *LibP2pNet) CancelDirectMsgHandle(chainId string, msgFlag string) error {
	ln.messageHandlerDistributor.cancelRegisterHandler(chainId, msgFlag) // remove stream handler for libP2pHost.
	return nil
}

// AddSeed add a seed node address. It can be a consensus node address.
func (ln *LibP2pNet) AddSeed(seed string) error {
	newSeedsAddrInfos, err := utils.ParseAddrInfo([]string{seed})
	if err != nil {
		return err
	}
	for _, info := range newSeedsAddrInfos {
		ln.libP2pHost.connManager.AddAsHighLevelPeer(info.ID)
	}

	if ln.startUp {
		seedPid, err := helper.GetNodeUidFromAddr(seed)
		if err != nil {
			return err
		}
		oldSeedsAddrInfos := ln.libP2pHost.connSupervisor.getPeerAddrInfos()
		for _, ai := range oldSeedsAddrInfos {
			if ai.ID.Pretty() == seedPid {
				ln.log.Warn("[Net] seed already exists. ignored.")
				return nil
			}
		}

		oldSeedsAddrInfos = append(oldSeedsAddrInfos, newSeedsAddrInfos...)
		ln.libP2pHost.connSupervisor.refreshPeerAddrInfos(oldSeedsAddrInfos)
		return nil
	}
	ln.prepare.AddBootstrapsPeer(seed)
	return nil
}

// RefreshSeeds reset addresses of seed nodes with given.
func (ln *LibP2pNet) RefreshSeeds(seeds []string) error {
	newSeedsAddrInfos, err := utils.ParseAddrInfo(seeds)
	if err != nil {
		return err
	}
	ln.libP2pHost.connManager.ClearHighLevelPeer()
	for _, info := range newSeedsAddrInfos {
		ln.libP2pHost.connManager.AddAsHighLevelPeer(info.ID)
	}
	if ln.startUp {
		ln.libP2pHost.connSupervisor.refreshPeerAddrInfos(newSeedsAddrInfos)
		return nil
	}
	for _, seed := range seeds {
		ln.prepare.AddBootstrapsPeer(seed)
	}
	return nil
}

// AddTrustRoot add a root cert for chain.
func (ln *LibP2pNet) AddTrustRoot(chainId string, rootCertByte []byte) error {
	if ln.startUp {
		if ln.libP2pHost.isGmTls {
			_, err := gmtlssupport.AppendNewCertsToTrustRoots(ln.libP2pHost.gmTlsChainTrustRoots, chainId, rootCertByte)
			if err != nil {
				ln.log.Errorf("[Net] add trust root failed. %s", err.Error())
				return err
			}
		} else if ln.libP2pHost.isTls {
			_, err := tlssupport.AppendNewCertsToTrustRoots(ln.libP2pHost.tlsChainTrustRoots, chainId, rootCertByte)
			if err != nil {
				ln.log.Errorf("[Net] add trust root failed. %s", err.Error())
				return err
			}
		}
		return nil
	}
	ln.prepare.AddTrustRootCert(chainId, rootCertByte)
	return nil
}

func (ln *LibP2pNet) ReVerifyTrustRoots(chainId string) {
	if !ln.startUp {
		return
	}
	peerIdTlsCertMap := ln.libP2pHost.peerIdTlsCertStore.StoreCopy()
	if len(peerIdTlsCertMap) == 0 {
		return
	}

	// re verify exist peers
	existPeers := ln.libP2pHost.peerChainIdsRecorder.PeerIdsOfChain(chainId)
	for _, existPeerId := range existPeers {
		bytes, ok := peerIdTlsCertMap[existPeerId]
		if ok {
			if ln.libP2pHost.isGmTls {
				chainTrustRoots := ln.libP2pHost.gmTlsChainTrustRoots
				// tls cert exist, parse to cert
				cert, err := gmx509.ParseCertificate(bytes)
				if err != nil {
					ln.log.Errorf("[Net] [ReVerifyTrustRoots] parse tls cert failed. %s", err.Error())
					continue
				}
				// whether verify failed, if failed remove it
				if !chainTrustRoots.VerifyCertOfChain(chainId, cert) {
					ln.libP2pHost.peerChainIdsRecorder.RemovePeerChainId(existPeerId, chainId)
					if err = ln.removeChainPubSubWhiteList(chainId, existPeerId); err != nil {
						ln.log.Warnf("[Net] [ReVerifyTrustRoots] remove chain pub-sub white list failed, %s",
							err.Error())
					}
					ln.log.Infof("[Net] [ReVerifyTrustRoots] remove peer from chain, (pid: %s, chain id: %s)",
						existPeerId, chainId)
				}
				delete(peerIdTlsCertMap, existPeerId)
				continue
			} else if ln.libP2pHost.isTls {
				chainTrustRoots := ln.libP2pHost.tlsChainTrustRoots
				// tls cert exist, parse to cert
				cert, err := x509.ParseCertificate(bytes)
				if err != nil {
					ln.log.Errorf("[Net] [ReVerifyTrustRoots] parse tls cert failed. %s", err.Error())
					continue
				}
				// whether verify failed, if failed remove it
				if !chainTrustRoots.VerifyCertOfChain(chainId, cert) {
					ln.libP2pHost.peerChainIdsRecorder.RemovePeerChainId(existPeerId, chainId)
					if err = ln.removeChainPubSubWhiteList(chainId, existPeerId); err != nil {
						ln.log.Warnf("[Net] [ReVerifyTrustRoots] remove chain pub-sub white list failed, %s",
							err.Error())
					}
					ln.log.Infof("[Net] [ReVerifyTrustRoots] remove peer from chain, (pid: %s, chain id: %s)",
						existPeerId, chainId)
				}
				delete(peerIdTlsCertMap, existPeerId)
				continue
			}
		} else {
			ln.libP2pHost.peerChainIdsRecorder.RemovePeerChainId(existPeerId, chainId)
			ln.log.Infof("[Net] [ReVerifyTrustRoots] remove peer from chain, (pid: %s, chain id: %s)",
				existPeerId, chainId)
		}
	}
	// verify other peers
	for pid, bytes := range peerIdTlsCertMap {
		if ln.libP2pHost.isGmTls {
			chainTrustRoots := ln.libP2pHost.gmTlsChainTrustRoots
			cert, err := gmx509.ParseCertificate(bytes)
			if err != nil {
				ln.log.Errorf("[Net] [ReVerifyTrustRoots] re-verify tls cert failed. %s", err.Error())
				continue
			}
			// whether verify success, if success add it
			if chainTrustRoots.VerifyCertOfChain(chainId, cert) {
				ln.libP2pHost.peerChainIdsRecorder.AddPeerChainId(pid, chainId)
				if err = ln.addChainPubSubWhiteList(chainId, pid); err != nil {
					ln.log.Warnf("[Net] [ReVerifyTrustRoots] add chain pub-sub white list failed, %s",
						err.Error())
				}
				ln.log.Infof("[Net] [ReVerifyTrustRoots] add peer to chain, (pid: %s, chain id: %s)",
					pid, chainId)
			}
			continue
		} else if ln.libP2pHost.isTls {
			chainTrustRoots := ln.libP2pHost.tlsChainTrustRoots
			cert, err := x509.ParseCertificate(bytes)
			if err != nil {
				ln.log.Errorf("[Net] [ReVerifyTrustRoots] re-verify tls cert failed. %s", err.Error())
				continue
			}
			// whether verify success, if success add it
			if chainTrustRoots.VerifyCertOfChain(chainId, cert) {
				ln.libP2pHost.peerChainIdsRecorder.AddPeerChainId(pid, chainId)
				if err = ln.addChainPubSubWhiteList(chainId, pid); err != nil {
					ln.log.Warnf("[Net] [ReVerifyTrustRoots] add chain pub-sub white list failed, %s",
						err.Error())
				}
				ln.log.Infof("[Net] [ReVerifyTrustRoots] add peer to chain, (pid: %s, chain id: %s)",
					pid, chainId)
			}
		}
	}

	// close all connections of peers not belong to any chain
	for _, s := range ln.libP2pHost.peerChainIdsRecorder.PeerIdsOfNoChain() {
		pid, err := peer.Decode(s)
		if err != nil {
			continue
		}
		for c := ln.libP2pHost.connManager.GetConn(pid); c != nil; c = ln.libP2pHost.connManager.GetConn(pid) {
			_ = c.Close()
			ln.log.Infof("[Net] [ReVerifyTrustRoots] close connection of peer %s", s)
			time.Sleep(time.Second)
		}
	}
}

func (ln *LibP2pNet) removeChainPubSubWhiteList(chainId, pidStr string) error {
	if ln.startUp {
		v, ok := ln.pubSubs.Load(chainId)
		if ok {
			ps := v.(*LibP2pPubSub)
			pid, err := peer.Decode(pidStr)
			if err != nil {
				ln.log.Errorf("[Net] parse peer id string to pid failed. %s", err.Error())
				return err
			}
			return ps.RemoveWhitelistPeer(pid)
		}
	}
	return nil
}

func (ln *LibP2pNet) addChainPubSubWhiteList(chainId, pidStr string) error {
	if ln.startUp {
		v, ok := ln.pubSubs.Load(chainId)
		if ok {
			ps := v.(*LibP2pPubSub)
			pid, err := peer.Decode(pidStr)
			if err != nil {
				ln.log.Errorf("[Net] parse peer id string to pid failed. %s", err.Error())
				return err
			}
			return ps.AddWhitelistPeer(pid)
		}
	}
	return nil
}

func (ln *LibP2pNet) reloadChainPubSubWhiteList(chainId string) {
	if ln.startUp {
		go time.AfterFunc(10*time.Second, func() {
			v, ok := ln.pubSubs.Load(chainId)
			if ok {
				ps := v.(*LibP2pPubSub)
				for _, pidStr := range ln.libP2pHost.peerChainIdsRecorder.PeerIdsOfChain(chainId) {
					pid, err := peer.Decode(pidStr)
					if err != nil {
						ln.log.Errorf("[Net] parse peer id string to pid failed. %s", err.Error())
						continue
					}
					err = ps.AddWhitelistPeer(pid)
					if err != nil {
						ln.log.Errorf("[Net] add pub-sub white list failed. %s (pid: %s, chain id: %s)",
							err.Error(), pid, chainId)
						continue
					}
					ln.log.Infof("[Net] add peer to chain pub-sub white list, (pid: %s, chain id: %s)",
						pid, chainId)
				}

			}
		})
	}
}

// RefreshTrustRoots reset all root certs for chain.
func (ln *LibP2pNet) RefreshTrustRoots(chainId string, rootsCertsBytes [][]byte) error {
	if ln.startUp {
		if !ln.libP2pHost.isTls {
			ln.log.Warn("[Net] tls disabled. ignored.")
			return nil
		}
		if ln.libP2pHost.isGmTls {
			if !ln.libP2pHost.gmTlsChainTrustRoots.RefreshRootsFromPem(chainId, rootsCertsBytes) {
				return errors.New("refresh trust roots failed")
			}
			return nil
		}
		if !ln.libP2pHost.tlsChainTrustRoots.RefreshRootsFromPem(chainId, rootsCertsBytes) {
			return errors.New("refresh trust roots failed")
		}
		return nil
	}
	for _, certsByte := range rootsCertsBytes {
		ln.prepare.AddTrustRootCert(chainId, certsByte)
	}
	return nil
}

// IsRunning
func (ln *LibP2pNet) IsRunning() bool {
	ln.lock.RLock()
	defer ln.lock.RUnlock()
	return ln.startUp
}

// ChainNodesInfo
func (ln *LibP2pNet) ChainNodesInfo(chainId string) ([]*api.ChainNodeInfo, error) {
	result := make([]*api.ChainNodeInfo, 0)
	if ln.libP2pHost.isTls {
		// 1.find all peerIds of chain
		peerIds := make([]string, 0)
		peerIds = append(peerIds, ln.libP2pHost.host.ID().Pretty())
		peerIds = append(peerIds, ln.libP2pHost.peerChainIdsRecorder.PeerIdsOfChain(chainId)...)
		for _, peerId := range peerIds {
			// 2.find addr
			pid, _ := peer.Decode(peerId)
			addrs := make([]string, 0)
			if pid == ln.libP2pHost.host.ID() {
				for _, multiaddr := range ln.libP2pHost.host.Addrs() {
					addrs = append(addrs, multiaddr.String())
				}
			} else {
				conn := ln.libP2pHost.connManager.GetConn(pid)
				if conn == nil || conn.RemoteMultiaddr() == nil {
					continue
				}
				addrs = append(addrs, conn.RemoteMultiaddr().String())
			}

			// 3.find cert
			cert := ln.libP2pHost.peerIdTlsCertStore.GetCertByPeerId(peerId)
			result = append(result, &api.ChainNodeInfo{
				NodeUid:     peerId,
				NodeAddress: addrs,
				NodeTlsCert: cert,
			})
		}
	}
	return result, nil
}

// GetNodeUidByCertId
func (ln *LibP2pNet) GetNodeUidByCertId(certId string) (string, error) {
	nodeUid, err := ln.libP2pHost.certPeerIdMapper.FindPeerIdByCertId(certId)
	if err != nil {
		return "", err
	}
	return nodeUid, nil
}

func (ln *LibP2pNet) handlePubSubWhiteList() {
	ln.handlePubSubWhiteListOnAddC()
	ln.handlePubSubWhiteListOnRemoveC()
}

func (ln *LibP2pNet) handlePubSubWhiteListOnAddC() {
	go func() {
		onAddC := make(chan string, pubSubWhiteListChanCap)
		ln.libP2pHost.peerChainIdsRecorder.OnAddNotifyC(onAddC)
		go func() {
			for ln.IsRunning() {
				time.Sleep(time.Duration(pubSubWhiteListChanQuitCheckDelay) * time.Second)
			}
			close(onAddC)
		}()

		for str := range onAddC {
			//ln.log.Debugf("[Net] handling pubsub white list on add chan,get %s", str)
			peerIdAndChainId := strings.Split(str, "<-->")
			ps, ok := ln.pubSubs.Load(peerIdAndChainId[1])
			if ok {
				pubsub := ps.(*LibP2pPubSub)
				pid, err := peer.Decode(peerIdAndChainId[0])
				if err != nil {
					ln.log.Errorf("[Net] peer decode failed, %s", err.Error())
				}
				ln.log.Infof("[Net] add to pubsub white list(peer-id:%s, chain-id:%s)",
					peerIdAndChainId[0], peerIdAndChainId[1])
				err = pubsub.AddWhitelistPeer(pid)
				if err != nil {
					ln.log.Errorf("[Net] add to pubsub white list(peer-id:%s, chain-id:%s) failed, %s",
						peerIdAndChainId[0], peerIdAndChainId[1], err.Error())
				}
			}
		}
	}()
}

func (ln *LibP2pNet) handlePubSubWhiteListOnRemoveC() {
	go func() {
		onRemoveC := make(chan string, pubSubWhiteListChanCap)
		ln.libP2pHost.peerChainIdsRecorder.OnRemoveNotifyC(onRemoveC)
		go func() {
			for ln.IsRunning() {
				time.Sleep(time.Duration(pubSubWhiteListChanQuitCheckDelay) * time.Second)
			}
			close(onRemoveC)
		}()
		for str := range onRemoveC {
			peerIdAndChainId := strings.Split(str, "<-->")
			ps, ok := ln.pubSubs.Load(peerIdAndChainId[1])
			if ok {
				pubsub := ps.(*LibP2pPubSub)
				pid, err := peer.Decode(peerIdAndChainId[0])
				if err != nil {
					ln.log.Errorf("[Net] peer decode failed, %s", err.Error())
					continue
				}
				ln.log.Debugf("[Net] remove from pubsub white list(peer-id:%s, chain-id:%s)",
					peerIdAndChainId[0], peerIdAndChainId[1])
				err = pubsub.RemoveWhitelistPeer(pid)
				if err != nil {
					ln.log.Errorf("[Net] remove from pubsub white list(peer-id:%s, chain-id:%s) failed, %s",
						peerIdAndChainId[0], peerIdAndChainId[1], err.Error())
				}
			}
		}
	}()
}

// Start
func (ln *LibP2pNet) Start() error {
	ln.lock.Lock()
	defer ln.lock.Unlock()
	if ln.startUp {
		ln.log.Warn("[Net] net is running.")
		return nil
	}
	var err error
	// prepare blacklist
	err = ln.prepareBlackList()
	if err != nil {
		return err
	}
	// create libp2p options
	ln.libP2pHost.opts, err = ln.createLibp2pOptions()
	if err != nil {
		return err
	}
	// set max size for conn manager
	ln.libP2pHost.connManager.SetMaxSize(ln.prepare.maxPeerCountAllow)
	// set elimination strategy for conn manager
	ln.libP2pHost.connManager.SetStrategy(ln.prepare.peerEliminationStrategy)
	// start libP2pHost
	if err = ln.libP2pHost.Start(); err != nil {
		return err
	}
	ln.initPeerStreamManager()
	if err = ln.registerMsgHandle(); err != nil {
		return err
	}
	// pkt adapter
	if err = ln.initPktAdapter(); err != nil {
		return err
	}
	// priority controller
	ln.initPriorityController()
	ln.startUp = true

	// start handling NewTlsPeerChainIdsNotifyC
	if ln.libP2pHost.isTls && ln.libP2pHost.peerChainIdsRecorder != nil {
		ln.handlePubSubWhiteList()
	}
	// start pubsub
	var psErr error = nil
	ln.pubSubs.Range(func(_, value interface{}) bool {
		pubsub := value.(*LibP2pPubSub)
		if err = pubsub.Start(); err != nil {
			psErr = err
			return false
		}
		return true
	})
	if psErr != nil {
		return psErr
	}

	// setup discovery
	adis := make([]string, 0)
	for bp := range ln.prepare.bootstrapsPeers {
		adis = append(adis, bp)
	}
	if err = SetupDiscovery(ln.libP2pHost, true, adis, ln.log); err != nil {
		return err
	}

	return nil
}

// Stop
func (ln *LibP2pNet) Stop() error {
	ln.lock.Lock()
	defer ln.lock.Unlock()
	if !ln.startUp {
		ln.log.Warn("[Net] net is not running.")
		return nil
	}
	if ln.pktAdapter != nil {
		ln.pktAdapter.cancel()
	}
	err := ln.libP2pHost.Stop()
	if err != nil {
		return err
	}
	ln.startUp = false

	return nil
}

func (ln *LibP2pNet) AddAC(chainId string, ac api.AccessControlProvider) {
	ln.libP2pHost.memberStatusValidator.AddAC(chainId, ac)
}

func (ln *LibP2pNet) CheckRevokeTlsCerts(ac api.AccessControlProvider, certManageSystemContractPayload []byte) error {
	var payload commonPb.Payload
	err := proto.Unmarshal(certManageSystemContractPayload, &payload)
	if err != nil {
		return fmt.Errorf("resolve payload failed: %v", err)
	}
	switch payload.Method {
	case syscontract.CertManageFunction_CERTS_REVOKE.String():
		return ln.checkRevokeTlsCertsCertsRevokeMethod(ac, &payload)
	default:
		return nil
	}
}

func (ln *LibP2pNet) checkRevokeTlsCertsCertsRevokeMethod(
	ac api.AccessControlProvider,
	payload *commonPb.Payload) error {
	// get all node tls cert
	peerIdCertBytesMap := ln.libP2pHost.peerIdTlsCertStore.StoreCopy()
	if len(peerIdCertBytesMap) == 0 {
		return nil
	}
	peerIdCertMap, err := common.ParsePeerIdCertBytesMapToPeerIdCertMap(peerIdCertBytesMap)
	if err != nil {
		ln.log.Errorf("[Net] parse bytes to cert failed, %s", err.Error())
		return err
	}
	return ln.checkRevokeTlsCertsCertsRevokeMethodRevokePeerId(ac, payload, peerIdCertMap)
}

func (ln *LibP2pNet) checkRevokeTlsCertsCertsRevokeMethodRevokePeerId(
	ac api.AccessControlProvider,
	payload *commonPb.Payload,
	peerIdCertMap map[string]*cmx509.Certificate) error {
	for _, param := range payload.Parameters {
		if param.Key == "cert_crl" {
			crlStr := strings.Replace(string(param.Value), ",", "\n", -1)
			_, err := ac.VerifyRelatedMaterial(pbac.VerifyType_CRL, []byte(crlStr))
			if err != nil {
				ln.log.Errorf("[Net] validate crl failed, %s", err.Error())
				return err
			}

			var crlList []*pkix.CertificateList

			crl, err := x509.ParseCRL([]byte(crlStr))
			if err != nil {
				ln.log.Errorf("[Net] validate crl failed, %s", err.Error())
				return err
			}
			crlList = append(crlList, crl)
			if err != nil {
				ln.log.Errorf("[Net] validate crl failed, %s", err.Error())
				return err
			}
			revokedPeerIds := ln.findRevokedPeerIdsByCRLs(crlList, peerIdCertMap)
			if err := ln.closeRevokedPeerConnection(revokedPeerIds); err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}

func (ln *LibP2pNet) findRevokedPeerIdsByCRLs(
	crlList []*pkix.CertificateList,
	peerIdCertMap map[string]*cmx509.Certificate) []string {
	revokedPeerIds := make([]string, 0)
	for _, crl := range crlList {
		for _, rc := range crl.TBSCertList.RevokedCertificates {
			for pid := range peerIdCertMap {
				cert := peerIdCertMap[pid]
				if rc.SerialNumber.Cmp(cert.SerialNumber) == 0 {
					revokedPeerIds = append(revokedPeerIds, pid)
				}
			}
		}
	}
	return revokedPeerIds
}

func (ln *LibP2pNet) closeRevokedPeerConnection(revokedPeerIds []string) error {
	for idx := range revokedPeerIds {
		pid := revokedPeerIds[idx]
		ln.log.Infof("[Net] revoked peer found(pid: %s)", pid)
		peerId, err := peer.Decode(pid)
		if err != nil {
			ln.log.Errorf("[Net] decode peer id failed, %s", err.Error())
			return err
		}
		ln.libP2pHost.memberStatusValidator.AddPeerId(pid)
		if ln.libP2pHost.connManager.IsConnected(peerId) {
			conn := ln.libP2pHost.connManager.GetConn(peerId)
			_ = conn.Close()
			ln.log.Infof("[Net] closing revoked peer connection(pid: %s)", pid)
		}
	}
	return nil
}

func (ln *LibP2pNet) SetMsgPriority(msgFlag string, priority uint8) {
	if ln.priorityController != nil {
		ln.priorityController.SetPriority(msgFlag, priorityblocker.Priority(priority))
	}
}
