/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tls

import (
	"chainmaker.org/chainmaker/chainmaker-net-libp2p/utils"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/sec"
	"net"
)

// ID is the protocol ID (used when negotiating with multistream)
const ID = "/tls/1.0.0"

// Transport constructs secure communication sessions for a peer.
type Transport struct {
	config *tls.Config

	privKey   crypto.PrivKey
	localPeer peer.ID
}

var _ sec.SecureTransport = &Transport{}

// New return a function can create a new Transport instance.
func New(
	tlsCfg *tls.Config,
) func(key crypto.PrivKey) (*Transport, error) {
	return func(key crypto.PrivKey) (*Transport, error) {
		id, err := peer.IDFromPrivateKey(key)
		if err != nil {
			return nil, err
		}
		return &Transport{
			config:    tlsCfg,
			privKey:   key,
			localPeer: id,
		}, nil
	}
}

// SecureInbound runs the TLS handshake as a server.
func (t *Transport) SecureInbound(ctx context.Context, insecure net.Conn) (sec.SecureConn, error) {
	c := tls.Server(insecure, t.config.Clone())
	if err := c.Handshake(); err != nil {
		insecure.Close()
		return nil, err
	}

	remotePubKey, err := t.getPeerPubKey(c)
	if err != nil {
		return nil, err
	}

	return t.setupConn(c, remotePubKey)
}

// SecureOutbound runs the TLS handshake as a client.
func (t *Transport) SecureOutbound(ctx context.Context, insecure net.Conn, p peer.ID) (sec.SecureConn, error) {
	c := tls.Client(insecure, t.config.Clone())
	if err := c.Handshake(); err != nil {
		insecure.Close()
		return nil, err
	}

	remotePubKey, err := t.getPeerPubKey(c)
	if err != nil {
		return nil, err
	}

	return t.setupConn(c, remotePubKey)
}

func (t *Transport) getPeerPubKey(conn *tls.Conn) (crypto.PubKey, error) {
	state := conn.ConnectionState()
	if len(state.PeerCertificates) <= 0 {
		return nil, errors.New("expected one certificates in the chain")
	}

	pubKey, err := utils.ParsePublicKeyToPubKey(state.PeerCertificates[0].PublicKey)
	if err != nil {
		return nil, fmt.Errorf("unmarshalling public key failed: %s", err)
	}
	return pubKey, err
}

func (t *Transport) setupConn(tlsConn *tls.Conn, remotePubKey crypto.PubKey) (sec.SecureConn, error) {
	remotePeerID, err := peer.IDFromPublicKey(remotePubKey)
	if err != nil {
		return nil, err
	}

	return &conn{
		Conn:         tlsConn,
		localPeer:    t.localPeer,
		privKey:      t.privKey,
		remotePeer:   remotePeerID,
		remotePubKey: remotePubKey,
	}, nil
}
