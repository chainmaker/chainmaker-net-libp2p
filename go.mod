module chainmaker.org/chainmaker/chainmaker-net-libp2p

go 1.15

require (
	chainmaker.org/chainmaker/chainmaker-net-common v0.0.5
	chainmaker.org/chainmaker/common/v2 v2.0.1-0.20210906085649-78f6202d8d60
	chainmaker.org/chainmaker/libp2p-pubsub v0.0.3
	chainmaker.org/chainmaker/pb-go/v2 v2.0.0
	chainmaker.org/chainmaker/protocol/v2 v2.0.1-0.20210906092203-47d66f4908f7
	github.com/gogo/protobuf v1.3.2
	github.com/libp2p/go-libp2p v0.11.0
	github.com/libp2p/go-libp2p-circuit v0.3.1
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/libp2p/go-libp2p-discovery v0.5.0
	github.com/libp2p/go-libp2p-kad-dht v0.10.0
	github.com/libp2p/go-libp2p-tls v0.1.3
	github.com/multiformats/go-multiaddr v0.3.2
	github.com/stretchr/testify v1.7.0
	github.com/tjfoc/gmsm v1.4.1
)

replace github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v0.0.2
